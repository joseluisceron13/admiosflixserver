# AdmiosFlix project
 
# Description
 
This project is ment to handle the information of the movie database.

 
#### Table of Contents
 
- [Server](#server)
	- [Installation](#installation)
	- [Quick Start](#quickstart)
	- [Server usage](#usageserver)
	- [Docker](#docker)
	- [Docs](#docs)
	- [What is included](#whatisincluded)
- [Frontend](#frontend)


 
# Server
 
#### Installation
- [Install nvm](https://github.com/nvm-sh/nvm) in order to use different versions of node

``` bash
# clone the repo
$ git clone https://gitlab.com/joseluisceron13/admiosflixserver.git
 
# go into app's directory
$ cd admionflixserver
 
# use node version 12.13.0
$ nvm install 12.13.0
$ nvm use 12.13.0
 
# install app's dependencies
$ npm install
```
 
#### Quick Start
- Create `./config` folder for .env files
- [Download the .env files](https://drive.google.com/drive/folders/11hdAq5NsbVc6LdLM2SURlSWIr5_rlBY0?usp=sharing) and place them in `./config` folder

```
admiosflixserver
├── config/         # environment variables files
├── src/            
└── package.json
...
```

- This project has been configured with a database deployed in a postgresql instance (GCP), if you want to use it e-mail me at: joseluisceron13@gmail.com with your public ip to active your connection

or 

- Go to `./sql` folder and execute  `db.sql` script to create the database if required. Then update credentials in .env files located in `./config` folder


```
admiosflixserver
├── sql/
│   └── db.sql    # data base script
│
├── config/        # .env files
└── package.json
...
```

#### Server usage
 
``` bash
# serve at localhost:3000 with automatically restart when saving
npm run dev
 
# run unit tests
npm run test
```

#### Docker
 
``` bash
# clone image
docker pull dockerjctest1/node_restapi_img
 
# run 
docker run -it -p 3000:4000 dockerjctest1/node_restapi_img

```

Keep in mind that this component points to a database located in the cloud. You must send your public IP to the email: joseluisceron13@gmail.com


#### Docs
- View the API docs from [here](http://localhost:3000/api/movies/api-docs)
 
#### What is included

Within the download you will find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You will see something like this:

```
admiosflixserver
├── config/          # .env files
│
├── sql/             # db script
│
├── src/             # project root
│   ├── controllers/  # functions to execute when calling routes
│   ├── database/    # database functions
│   ├── models/       # database models
│   ├── routes/        # server routes
│   ├── utils/            # general functions
│   └── app.js          # server configuration
│   └── index.js        # starting application file
│   └── openapi.js    # API documentation
│
├── test/             # tests functions
│
├── .babelrc  # local configuration file  
│
└── package.json
```

## Frontend
- Go to this [repository](https://gitlab.com/joseluisceron13/admiosflixweb.git) to execute the front end project

### Features and technologies

- Express: to set api logic
- moment: parse, validate, manipulate, and display dates and times in JavaScript.
- babel/polyfill: adds support to the web browsers for features, which are not available
- sequelize: sequelize provides various methods to assist querying your database for data.
- dotenv: loads environment variables from .env files
- supertest: library for testing HTTP servers
- swagger-ui-express: for API documentation
- async functions: Async functions return a Promise by default, so you can rewrite any callback based function to use Promises, then await their resolution
- Postresql instance using GCP


**Table of Contents**

[TOCM]

[TOC]






