module.exports = {
  "openapi": "3.0.1",
  "info": {
    "title": "API for admiosFlix company",
    "description": "This API is in charge of handling the information of the movie database.",
    "version": "1.0.0",
    "contact": {
      "name": "Jose Luis Cerón",
      "email": "joseluisceron13@gmail.com"
    }
  },
  "servers": [
    {
      "url": "http://localhost:3000/api/movies"
    }
  ],
  "tags": [
    {
      "name": "api/movies",
      "description": "This endpoint handles all the information related to movies."
    }
  ],
  "paths": {
    "/": {
      "post": {
        "tags": [
          "api/movies"
        ],
        "summary": "Inserts the information of a new movie to the database.",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/BodyMoviePost"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "(OK) The movie information has been saved.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SuccessMoviePost"
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/BadRequest"
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      },
      "get": {
        "tags": [
          "api/movies"
        ],
        "summary": "Gets movies from database grouped by genre and sorted by title",
        "responses": {
          "200": {
            "description": "(OK) The movies information was got correctly.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SuccessMoviesGet"
                }
              }
            }
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    },
    "/catalog": {
      "post": {
        "tags": [
          "api/movies"
        ],
        "summary": "Gets the catalog of movies by genre",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/BodyMoviesByGenrePost"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "(OK) The movie information has been saved.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SuccessMoviesGet"
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/BadRequest"
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    }
  },
  "components": {
    "responses": {
      "BadRequest": {
        "description": "(Bad Request) the data sent is incorrect or there are mandatory data not sent"
      },
      "InternalServerError": {
        "description": "(Internal server error) something goes wrong"
      }
    },
    "schemas": {
      "BodyMoviePost": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "description": "Name of the movie"
          },
          "rating": {
            "type": "integer",
            "minimum": 1,
            "maximum": 5,
            "description": "Movie rating"
          },
          "genre_id": {
            "type": "integer",
            "minimum": 1,
            "maximum": 6,
            "description": "id of genre to which the movie belongs"
          },
          "cast_crew": {
            "type": "object",
            "properties": {
              "director": {
                "type": "string"
              },
              "writingCredits": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              "musicBy": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              }
            }
          },
          "release_date": {
            "type": "string",
            "description": "Must be send in YYYY-MM-DD or YYYYMMDD format"
          }
        }
      },
      "BodyMoviesByGenrePost": {
        "type": "object",
        "properties": {
          "genre_id": {
            "type": "integer",
            "minimum": 1,
            "maximum": 6
          },
          "perPage": {
            "type": "integer",
            "minimum": 1
          }
        }
      },
      "SuccessMoviePost": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string",
            "description": "Success message \"Movie created!\""
          },
          "data": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer"
              },
              "title": {
                "type": "string"
              },
              "rating": {
                "type": "integer"
              },
              "genre_id": {
                "type": "integer"
              },
              "cast_crew": {
                "type": "object",
                "properties": {
                  "director": {
                    "type": "string"
                  },
                  "writingCredits": {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  },
                  "musicBy": {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                }
              },
              "release_date": {
                "type": "string"
              }
            }
          }
        }
      },
      "SuccessMoviesGet": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string"
          },
          "data": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "count": {
                  "type": "integer"
                },
                "genre_id": {
                  "type": "integer"
                },
                "genre": {
                  "type": "string"
                },
                "perPage": {
                  "type": "integer"
                },
                "movies": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "integer"
                      },
                      "title": {
                        "type": "string",
                        "description": "Name of the movie"
                      },
                      "rating": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 5,
                        "description": "Movie rating"
                      },
                      "cast_crew": {
                        "type": "object",
                        "properties": {
                          "director": {
                            "type": "string"
                          },
                          "writingCredits": {
                            "type": "array",
                            "items": {
                              "type": "string"
                            }
                          },
                          "musicBy": {
                            "type": "array",
                            "items": {
                              "type": "string"
                            }
                          }
                        }
                      },
                      "release_date": {
                        "type": "string",
                        "description": "Release date of the movie"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}