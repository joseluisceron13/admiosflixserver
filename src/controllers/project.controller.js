const Movie = require("../models/Movie");
const Genre = require("../models/Genre");
const { setCatalog, validateFields } = require("../utils/utils");


const createMovie = async (req, res) => {
  const { isValid, msg} = validateFields(req.body);
  if (!isValid) {
    return res.status(400).json({
      message: msg,
      data: null
    });
  }
  const { title, rating, genre_id, cast_crew, release_date } = req.body;

  const movieParams = {
    title,
    rating,
    genre_id,
    cast_crew: JSON.stringify(cast_crew),
    release_date
  };

  try {
    let movieFound = await Movie.findOne({ where: { title } });

    if (movieFound) {
      res.status(400).json({
        message: "The movie title is already registered",
        data: null
      });
    } else {
      let movieCreated = await Movie.create(movieParams);
      return res.json({
        message: "Movie created!",
        data: movieCreated
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Something goes wrong",
      data: null
    });
  }
};

const getAllMovies = async(req, res) => {
  
  let offset = 0
  let perPage =3
 
  try {
    
    let genresPayload = await Genre.findAll({
      attributes: ["id", "name"],
      order: [
      ["name", "ASC"]
    ]})
    let genres = JSON.parse(JSON.stringify(genresPayload))

    let catalogPromises = []
    genres.map(genre => {
      let itemPromise = Movie.findAndCountAll({
        where: {genre_id: genre.id},
        offset,
        limit: perPage,
        attributes: ["id", "title", "rating", "cast_crew", "release_date"],
        order: [
          ["title", "ASC"]
        ]
      });
      catalogPromises.push(itemPromise)
    })

    return Promise.all(catalogPromises).then(response => {
      let catalog = setCatalog(response, genres, offset, perPage)
      return res.json({
        message: "Executed",
        data: catalog
      });
    }).catch(error => {
      return res.status(500).json({
        message: "Something goes wrong in Promise.all",
        data: null
      });
    })

  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Something goes wrong",
      data: null
    });
  }
};

const getMoviesByGenre = async (req, res) => {
  const { genre_id, perPage } = req.body;
  try {
    let catalog = await Movie.findAndCountAll({
      where: {genre_id},
      offset:0,
      limit: perPage,
      include: [Genre],
      attributes: ["id", "title", "rating", "cast_crew", "release_date"],
      order: [
        ["title", "ASC"]
      ]
    });    
    if(catalog){    
      catalog["movies"] = catalog.rows

      catalog.movies.map(movie => {
        movie.cast_crew = JSON.parse(movie.cast_crew)
      })
      delete catalog["rows"]
      catalog["genre_id"] = genre_id
      catalog["genre"] = catalog.movies[0].genre.name
      catalog["perPage"] = perPage
    }

    return res.json({
      message: "Executed",
      data: catalog
    })
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Something goes wrong",
      data: null
    });
  }
};

const deleteMovie = async (req, res) => {
  const { id } = req.params;
  try {
    const deleteRowCount = await Movie.destroy({ where: { id } });

    res.json({
      message: "Movie deleted succesfully!",
      count: deleteRowCount
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Something goes wrong",
      data: null
    });
  }
};

const updateMovie = async (req, res) => {
  const { id } = req.params;
  const { title, rating, genre_id, cast_crew, release_date } = req.body;
  const movieParams = {
    title,
    rating,
    genre_id,
    cast_crew: JSON.stringify(cast_crew),
    release_date
  };
  try {
    const movies = await Movie.findAll({
      where: { id },
      include: [Genre],
      attributes: ["id", "title", "rating", "cast_crew", "release_date"]
    });

    if (movies.length > 0) {
      movies.forEach(async movie => {
        await movie.update(movieParams);
      });
    }

    res.json({
      message: "Movie updated succesfully!",
      data: movies
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Something goes wrong",
      data: null
    });
  }
};

module.exports = {
  createMovie,
  getAllMovies,
  deleteMovie,
  updateMovie,
  getMoviesByGenre,
};
