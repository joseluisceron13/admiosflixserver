const express = require("express");
const morgan = require("morgan");

//  importing routes
const movieRoutes = require("./routes/movies");

// initialization
const app = express();

//  middlewares
app.use(morgan("dev"));
app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

//  routes
app.use("/api/movies", movieRoutes);

module.exports = app;
