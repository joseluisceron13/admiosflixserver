const moment = require('moment');

const setCatalog = (catalog, genres, offset, perPage) => {
  catalog.map((item, i) => {
    item["genre_id"] = genres[i].id;
    item["genre"] = genres[i].name;
    item["movies"] = item.rows;
    delete item['rows'];
    item["nexToken"] = offset + item.count;
    item["perPage"] = perPage

    item.movies.map(movie => {
      movie.cast_crew = JSON.parse(movie.cast_crew )
    })
    
  });
  return catalog;
};

const validateFields = body => {
  let msgNull = "You should implement the ";
  let msgEmptyString = "The following variable can not be a empty string: ";
  let msgIsNegative = "The folllowin variable can not be negative: ";
  let msgIsBiggerThan = "5 is the maximun value for the following variable: ";
  let msgIsNotDate = "The following variable should be YYYY-MM-DD or YYYYMMDD format: "
  let msgEmptyObject = "The following variable can not be empty object: "

  // title
  if (body.title == null) {
    return { isValid: false, msg: msgNull + "title" };
  }
  if (body.title == "") {
    return { isValid: false, msg: msgEmptyString + "title" };
  }
  // rating
  if (body.rating == null) {
    return { isValid: false, msg: msgNull + "rating" };
  }
  if (body.rating == "") {
    return { isValid: false, msg: msgEmptyString + "rating" };
  }
  if (body.rating < 0) {
    return { isValid: false, msg: msgIsNegative + "rating" };
  }
  if (body.rating > 5) {
    return { isValid: false, msg: msgIsBiggerThan + "rating" };
  }
  
  // genre_id
  if (body.genre_id == null) {
    return { isValid: false, msg: msgNull + "genre_id" };
  }
  // realease_date
  if (body.release_date == null) {
    return { isValid: false, msg: msgNull + "release_date" };
  }
  if (!isValidDate(body.release_date)) {
    return { isValid: false, msg: msgIsNotDate + "release_date" };
  }

  // cast_crew validations
  if (body.cast_crew == null) {
    return { isValid: false, msg: msgNull + "cast_crew" };
  }
  if (body.cast_crew == "") {
    return { isValid: false, msg: msgEmptyString + "cast_crew" };
  }
  if (Object.keys(body.cast_crew).length == 0) {
    return { isValid: false, msg: msgEmptyObject + "cast_crew" };
  }

  // director
  if (body.cast_crew.director == null) {
    return { isValid: false, msg: msgNull + "director" };
  }
  if (body.cast_crew.director == "") {
    return { isValid: false, msg: msgEmptyString + "director" };
  }

  // cast
  // if (body.cast_crew.cast == null) {
  //   return { isValid: false, msg: msgNull + "cast" };
  // }
  // if (body.cast_crew.cast == "") {
  //   return { isValid: false, msg: msgEmptyString + "cast" };
  // }
  // writingCredits 
  if (body.cast_crew.writingCredits == null) {
    return { isValid: false, msg: msgNull + "writingCredits" };
  }
  if (body.cast_crew.writingCredits == "") {
    return { isValid: false, msg: msgEmptyString + "writingCredits" };
  }
  // musicBy
  if (body.cast_crew.musicBy == null) {
    return { isValid: false, msg: msgNull + "musicBy" };
  }
  if (body.cast_crew.musicBy == "") {
    return { isValid: false, msg: msgEmptyString + "musicBy" };
  }




  
  if (body.cast_crew.writingCredits == "") {
    return { isValid: false, msg: msgEmptyString + "writingCredits" };
  }
 

  return { isValid: true, field: null };
};

const isValidDate = (date) => {
  let validation1 = moment(date, 'YYYY-MM-DD',true).isValid()
  let validation2 = moment(date, 'YYYYMMDD',true).isValid()

  if(validation1 || validation2) return true
  else return false
  
}

module.exports = {
  setCatalog,
  validateFields
};
