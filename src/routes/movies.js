const express = require("express");
const router = new express.Router();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../openapi.js');
const {
  createMovie,
  getAllMovies,
  deleteMovie,
  updateMovie,
  getMoviesByGenre,
} = require("../controllers/project.controller");

//  /api/movies
router.get("/", getAllMovies);
router.post("/", createMovie);
router.delete("/:id", deleteMovie);
router.put("/:id", updateMovie);
router.post("/catalog", getMoviesByGenre);

// API documentation
router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerDocument));

module.exports = router;
