require("dotenv").config();
const { Sequelize, DataTypes, Model } = require("sequelize");

const HOST = process.env.HOST;
const DB_USER = process.env.DB_USER;
const PASSWORD = process.env.PASSWORD;
const DB = process.env.DB;

let sequelizeConnector = new Sequelize(DB, DB_USER, PASSWORD, {
  host: HOST,
  dialect: "postgres",
  logging: false
});

sequelizeConnector
  .authenticate()
  .then(_ => {
    console.log("Authenticated");
  })
  .catch(error => {
    console.log("Error Authentication");
    console.log(error);
  });

module.exports = sequelizeConnector;
