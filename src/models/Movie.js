const { DataTypes } = require("sequelize");
const sequelizeConnector = require("../database/db");
const Genre = require("./Genre");

const Movie = sequelizeConnector.define(
  "movies",
  {
    rating: { type: DataTypes.INTEGER, allowNull: false },
    genre_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: Genre, key: "id" }
    },
    cast_crew: { type: DataTypes.STRING, allowNull: true },
    release_date: { type: DataTypes.DATE, allowNull: false },
    title: { type: DataTypes.STRING, allowNull: false }
  },
  {
    tableName: "movies",
    underscored: true,
    timestamps: true,
    createdAt: false,
    updatedAt: false,
    deletedAt: false
  }
);

Movie.belongsTo(Genre, { foreignKey: "genre_id" });

module.exports = Movie;
