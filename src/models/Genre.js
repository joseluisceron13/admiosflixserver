const { DataTypes } = require("sequelize");
const sequelizeConnector = require("../database/db");

const Genre = sequelizeConnector.define(
  "genres",
  {
    name: { type: DataTypes.STRING, allowNull: false }
  },
  {
    tableName: "genres",
    underscored: true,
    timestamps: true,
    createdAt: false,
    updatedAt: false,
    deletedAt: false
  }
);

module.exports = Genre;
