const request = require("supertest");
const app = require("../src/app");

let movie = {
  title: "Avatar2",
  rating: 5,
  genre_id: 5,
  cast_crew: {
    director: "James Cameron",
    writingCredits: ["James Cameron"],
    cast: [
      {
        name: "Sam Worthington",
        jobTitle: "Jake Sully"
      },
      {
        name: "Zoe Saldana",
        jobTitle: "Neytiri"
      }
    ],
    musicBy: ["James Horner"]
  },
  release_date: "2009-12-17"
};

test("Get all movies", done => {
  let body = { offset: 0, perPage: 10 };
  request(app)
    .get("/api/movies")
    .send(body)
    .then(result => {
      expect(result.status).toBe(200);
      expect(result.body.message).toBe("Executed");
      done();
    });
});

test("Not create a movie, missing title", done => {
  let movieClone = { ...movie };
  delete movieClone.title;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("You should implement the title");
      done();
    });
});

test("Not create a movie, title empty string", done => {
  let movieClone = { ...movie };
  movieClone["title"] = "";
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The following variable can not be a empty string: title");
      done();
    });
});

test("Not create a movie, missing rating", done => {
  let movieClone = { ...movie };
  delete movieClone.rating;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("You should implement the rating");
      done();
    });
});

test("Not create a movie, rating empty string", done => {
  let movieClone = { ...movie };
  movieClone["rating"] = "";
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The following variable can not be a empty string: rating");
      done();
    });
});

test("Not create a movie, rating negative", done => {
  let movieClone = { ...movie };
  movieClone["rating"] = -3;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The folllowin variable can not be negative: rating");
      done();
    });
});

test("Not create a movie, rating bigger than 5", done => {
  let movieClone = { ...movie };
  movieClone["rating"] = 6;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("5 is the maximun value for the following variable: rating");
      done();
    });
});

test("Not create a movie, missing genre_id", done => {
  let movieClone = { ...movie };
  delete movieClone.genre_id;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("You should implement the genre_id");
      done();
    });
});

test("Not create a movie, missing release_date", done => {
  let movieClone = { ...movie };
  delete movieClone.release_date;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("You should implement the release_date");
      done();
    });
});

test("Not create a movie, release_date not in format YYYY-MM-DD or YYYYMMDD", done => {
  let movieClone = { ...movie };
  movieClone.release_date = "200-12-01";
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The following variable should be YYYY-MM-DD or YYYYMMDD format: release_date");
      done();
    });
});

test("Not create a movie, missing cast_crew", done => {
  let movieClone = { ...movie };
  delete movieClone.cast_crew;
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("You should implement the cast_crew");
      done();
    });
});

test("Not create a movie, cast_crew empty string", done => {
  let movieClone = { ...movie };
  movieClone["cast_crew"] = "";
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The following variable can not be a empty string: cast_crew");
      done();
    });
});
test("Not create a movie, cast_crew empty object", done => {
  let movieClone = { ...movie };
  movieClone["cast_crew"] = {};
  request(app)
    .post("/api/movies")
    .send(movieClone)
    .then(result => {
      expect(result.status).toBe(400);
      expect(result.body.message).toBe("The following variable can not be empty object: cast_crew");
      done();
    });
});
